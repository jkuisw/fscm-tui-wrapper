:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: tui-wrapper.scm
Implementes wrapper procedures for fluent text user interface (tui) commands.

:arrow_right: [Go to source code of this file.](/src/tui-wrapper.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: string-list->tui-list](#string-list-tui-list) | Converts a scheme list of strings (only strings!) to a fluent tui list, whic... |
| [:page_facing_up: execute-udf-function](#execute-udf-function) | Executes a udf function which where declared by the "DEFINE_ON_DEMAND(...)"m... |
| [:page_facing_up: define-custom-field-function](#define-custom-field-function) | Define a custom field function. If the force parameter is `#t`, than it will... |
| [:page_facing_up: display-surface-mesh](#display-surface-mesh) | Displays the surface mesh for the given surfaces. Tui commands used: + `/d... |
| [:page_facing_up: set-udm-size](#set-udm-size) | Set the size of the user defined memory. Tui command used: `/define/user-de... |
| [:page_facing_up: read-case-file](#read-case-file) | Reads the given case file (*.cas). Tui command used: `/file/read-case` |
| [:page_facing_up: read-case-and-data](#read-case-and-data) | Reads the data file (\*.dat) and the corresponding case file (\*.cas). Tui ... |
| [:page_facing_up: create-iso-surface](#create-iso-surface) | Creates a new iso surface. if an iso surface with the given name alreadyexis... |
| [:page_facing_up: xy-plot-to-file](#xy-plot-to-file) | Writes the x-y plot values to the given file. Tui command used: `/plot/plot... |
| [:page_facing_up: read-views](#read-views) | Reads saved fluent views from a file (*.vw) and imports them. Tui command u... |
| [:page_facing_up: save-picture](#save-picture) | Procedure for saving pictures of the active window and the current view. Tu... |
| [:page_facing_up: contour-plot](#contour-plot) | Generates a contour plot of the given type. Tui commands used: + `/display... |

## Procedure Documentation

### string-list->tui-list

#### Syntax
```scheme
(string-list->tui-list string-list)
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L20)

#### Export Name
`<import-name>/string-list->tui-list`

#### Description
Converts a scheme list of strings (only strings!) to a fluent tui list, which
can be used for tui commands a s input.  
Example lists:  
+ scheme list: `(list "str1" "str2" "str3")`
+ tui list: `str1 str2 str3 '()`

#### Parameters
##### `string-list`  
The valid scheme list of strings that should be converted to
the fluent tui list.


#### Returns
The fluent tui list.

-------------------------------------------------
### execute-udf-function

#### Syntax
```scheme
(execute-udf-function libname funcname)
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L38)

#### Export Name
`<import-name>/execute-udf-function`

#### Description
Executes a udf function which where declared by the "DEFINE_ON_DEMAND(...)"
macro.  
Tui command used: `/define/user-defined/execute-on-demand`

#### Parameters
##### `libname`  
The name of the library of the udf function to execute,
e.g.: "libudf"

##### `funcname`  
The name of the function to execute, this is the parameter of
the "DEFINE_ON_DEMAND(...)" macro.  
Example: `DEFINE_ON_DEMAND(calc_efficiency) { ... }` -> "calc_efficiency"



-------------------------------------------------
### define-custom-field-function

#### Syntax
```scheme
(define-custom-field-function name code [force])
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L635)

#### Export Name
`<import-name>/define-custom-field-function`

#### Description
Define a custom field function. If the force parameter is `#t`, than it will
be checked if the passed custom field function name already exists. When this
is the case, than the existing custom field function will be deleted.  
Tui commands used: `/define/custom-field-function/delete` and
`/define/custom-field-function/define`

#### Parameters
##### `name`  
The name for the custom field function, must be a string or symbol.

##### `code`  
The code for the custom field function, must be a string or symbol.

##### `force`  
_Attributes: optional, default: `#f`_  
When passed `#t`, than existing custom field
function with the same name will be deleted before creation of the new
one.



-------------------------------------------------
### display-surface-mesh

#### Syntax
```scheme
(display-surface-mesh surfaces [options])
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L751)

#### Export Name
`<import-name>/display-surface-mesh`

#### Description
Displays the surface mesh for the given surfaces.  
Tui commands used:  
+ `/display/set` - base command for configuration, see `options` parameter
+ `/display/surface-mesh` - command to display surface mesh  
 
Example:  
```scheme
(<import-name>/display-surface-mesh
  (list 'surface-name-1 'surface-name-2)
  (list ; command options (below are the default options)
    "filled-mesh? yes" ; Options: Faces (yes/no)
    ; Options: Edges (yes/no), if edges yes - Edge Type:
    ;  All edges? (yes/no), if All edges? no:
    ;    Outline edges? (yes/no)
    ;    Feature edges? (yes/no), if Feature edges? yes:
    ;      Feature angle? (number)
    "rendering-options/surface-edge-visibility yes yes"
    "mesh-partitions? no" ; Options: Partitions (yes/no)
    "element-shrink 0" ; Shrink Factor
  )
)
```

#### Parameters
##### `surfaces`  
A list of surface names, for which the surface mesh should be
displayed.

##### `options`  
_Attributes: optional, default: `(list "filled-mesh? yes" "rendering-options/surface-edge-visibility yes yes" "mesh-partitions? no" "element-shrink 0")`_  
With this parameter the command can be configured. The options are a list
of strings. Each string will be appended to the options base command,
which is the string "/display/set". The combined string defines than a tui
command that will be executed to configure the display surface mesh
command. So the following example for the command `options`  
```scheme
(list ; command options (below are the default options)
  "filled-mesh? yes" ; Options: Faces (yes/no)
  ; Options: Edges (yes/no), if edges yes - Edge Type:
  ;  All edges? (yes/no), if All edges? no:
  ;    Outline edges? (yes/no)
  ;    Feature edges? (yes/no), if Feature edges? yes:
  ;      Feature angle? (number)
  "rendering-options/surface-edge-visibility yes yes"
  "mesh-partitions? no" ; Options: Partitions (yes/no)
  "element-shrink 0" ; Shrink Factor
)
```
will expand to the following four tui commands:  
1. `/display/set/filled-mesh? yes`
2. `/display/set/rendering-options/surface-edge-visibility yes yes`
3. `/display/set/mesh-partitions? no`
4. `/display/set/element-shrink 0`



-------------------------------------------------
### set-udm-size

#### Syntax
```scheme
(set-udm-size size)
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L52)

#### Export Name
`<import-name>/set-udm-size`

#### Description
Set the size of the user defined memory.  
Tui command used: `/define/user-defined/user-defined-memory`

#### Parameters
##### `size`  
The size of the user defined memory to set.



-------------------------------------------------
### read-case-file

#### Syntax
```scheme
(read-case-file case-file)
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L69)

#### Export Name
`<import-name>/read-case-file`

#### Description
Reads the given case file (*.cas).  
Tui command used: `/file/read-case`

#### Parameters
##### `case-file`  
The file path to the case file (directory and file name).



-------------------------------------------------
### read-case-and-data

#### Syntax
```scheme
(read-case-and-data dat-file)
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L89)

#### Export Name
`<import-name>/read-case-and-data`

#### Description
Reads the data file (*.dat) and the corresponding case file (*.cas).  
Tui command used: `/file/read-case-data`

#### Parameters
##### `dat-file`  
The file path to the data file (directory and file name).



-------------------------------------------------
### create-iso-surface

#### Syntax
```scheme
(create-iso-surface type name iso-values [from-surfaces] [from-zones])
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L131)

#### Export Name
`<import-name>/create-iso-surface`

#### Description
Creates a new iso surface. if an iso surface with the given name already
exists, than it will be deleted.  
Tui commands used: `/surface/delete-surface` and `/surface/iso-surface`  
 
Example:  
```scheme
(<import-name>/create-iso-surface
  "x-coordinate" ; iso value type
  surface-name ; iso-surface name
  (list 0.03851) ; iso values list
  (list 'laufrad_outer_interface) ; from surfaces list
)
```

#### Parameters
##### `type`  
The type of the iso value, for example `"x-coordinate"` or
`"radial-coordinate"`, passed as string. For a full list of possible
types, execute the tui command `/surface/iso-surface`, then when asked
`iso-surface of>` press enter and you get a list of possible types.

##### `name`  
The name of the new surface.

##### `iso-values`  
A list of iso values, the number of needed iso values depends
on the type.

##### `from-surfaces`  
_Attributes: optional, default: `'()`_  
A list of surface names, from which
the iso surface combined with the iso values will be created.

##### `from-zones`  
_Attributes: optional, default: `'()`_  
A list of zone names, which must be
symbols, from which the iso surface combined with the iso values will be
created.



-------------------------------------------------
### xy-plot-to-file

#### Syntax
```scheme
(xy-plot-to-file filepath x-function y-function surfaces [use-node-values] [order-points])
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L229)

#### Export Name
`<import-name>/xy-plot-to-file`

#### Description
Writes the x-y plot values to the given file.  
Tui command used: `/plot/plot`  
 
Example:  
```scheme
(<import-name>/xy-plot-to-file
  "/path/to/file.xy" ; file path
  "angular-coordinate" ; x cell function
  "pressure" ; y cell function
  (list surface-name) ; list of sufaces
)
```

#### Parameters
##### `filepath`  
The path to the file where the x-y plot values should be written
to.

##### `x-function`  
The x cell function for the plot operation, for example
`"x-coordinate"` or `"radial-coordinate"`. For a full list of possible
cell functions, execute the tui command `/plot/plot`, then when asked
`cell function>` press enter and you get a list of possible cell
functions.

##### `y-function`  
The y cell function for the plot operation, for example
`"pressure"` or `"velocity-mag"`. For a full list of possible cell
functions, execute the tui command `/plot/plot`, then when asked
`cell function>` press enter and you get a list of possible cell
functions.

##### `surfaces`  
A list of surface names for which the plot operation should
apply.

##### `use-node-values`  
_Attributes: optional, default: `#t`_  
When passed #t, then node values
will be used for the plot operation.

##### `order-points`  
_Attributes: optional, default: `#t`_  
When passed #t, then the values
written to the output file will be sorted by the coordinates fo the
points.



-------------------------------------------------
### read-views

#### Syntax
```scheme
(read-views filepath)
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L305)

#### Export Name
`<import-name>/read-views`

#### Description
Reads saved fluent views from a file (*.vw) and imports them.  
Tui command used: `/views/read-views`

#### Parameters
##### `filepath`  
The file path to the views file (file extension *.vw).



-------------------------------------------------
### save-picture

#### Syntax
```scheme
(save-picture filepath window view content-printer [picture-driver] [picture-options])
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L379)

#### Export Name
`<import-name>/save-picture`

#### Description
Procedure for saving pictures of the active window and the current view.  
Tui commands used:  
+ `/display/open-window` - to open a window
+ `/display/set-window` - for setting the active window
+ `/display/views/restore-view` - for restoring a saved view
+ `/display/set/picture` - base command for picture settings, see
  `picture-options` parameter
+ `/display/save-picture` - command to save the picture  
 
Example:  
```scheme
(<import-name>/save-picture
  "/path/to/the/picture.png" ; filepath of picture
  20 ; window number
  "name-of-view" ; view name
  (lambda () (begin ; content printer procedure
    ; print some content to the active window, e.g.: a contour plot
  ))
  "png" ; picture dirver (png is the default picture driver)
  (list ; picture driver options (below are the default options)
    (list "x-resolution" 1920)
    (list "y-resolution" 1080)
  )
)
```

#### Parameters
##### `filepath`  
File path to where the picture should be saved (directory and
file name).

##### `window`  
The number of the window from which the picture should be taken.

##### `view`  
The name of the view for the picture. Must be an existing view,
because the view will be restored in the active window befor taking the
picture.

##### `content-printer`  
The content-printer is a procedure that will be called,
after setting the active window and restoring the view and before taking
the picture. It is responsible for printing content to the window, such as
a contour plot of something, from which a picture should be saved.

##### `picture-driver`  
_Attributes: optional, default: `"png"`_  
The picture driver to use. You
can see the possible picture drivers when executing the tui command
`/display/set/picture/driver`.

##### `picture-options`  
_Attributes: optional, default: `(list (list "x-resolution" 960) (list "y-resolution" 720))`_  
With this parameter the picture dirver can be configured. The display
options are a list of key value pairs. Each key value pair is a setting
for the picture driver. The key is a string that will be appended to the
picture base command, which is the string "/display/set/picture/". The
combined string defines than a tui command and the value of the key value
pair defines the parameters for the tui command. So the following example
for `picture-options`  
```scheme
(list ; picture driver options (below are the default options)
  (list "x-resolution" 1920)
  (list "y-resolution" 1080)
)
```
will expand to the following two tui commands:  
1. `/display/set/picture/x-resolution 1920`
2. `/display/set/picture/y-resolution 1080`



-------------------------------------------------
### contour-plot

#### Syntax
```scheme
(contour-plot contour-of [surfaces] [options] [minimum] [maximum])
```

:arrow_right: [Go to source code of this procedure.](/src/tui-wrapper.scm#L525)

#### Export Name
`<import-name>/contour-plot`

#### Description
Generates a contour plot of the given type.  
Tui commands used:  
+ `/display/set/contours` - base command for contour plot settings, see
  `options` parameter
+ `/display/set/contours/surfaces` - define surfaces for the contour plot
+ `/display/contour` - command for creating contour plot  
 
Example:  
```scheme
(<import-name>/contour-plot
  "velocity-magnitude" ; type of contour plot
  (list 'iso-surf-z=0) ; surfaces for the contour plot
  (list ; contour plot options (below are the default options)
    (list "filled-contours?" "yes") ; Filled
    (list "node-values?" "yes") ; Node Values
    (list "global-range?" "no") ; Global Range
    (list "clip-to-range?" "no") ; Clip to Range
    (list "line-contours?" "no") ; Draw Profiles
    (list "render-mesh?" "no") ; Draw Mesh
    (list "n-contour" 20) ; Levels
    (list "log-scale?" "no")
  )
  0 ; range minimum value
  4.6e+1 ; range maximum value
)
```

#### Parameters
##### `contour-of`  
The type of the contour plot, for example `"pressure"` or
`"velocity-magnitude"`. For a full list of possible contour plot types,
execute the tui command `/display/contour`, then when asked
`contours of>` press enter and you get a list of possible contour plot
types.

##### `surfaces`  
_Attributes: optional, default: `'()`_  
A list of surface names, for which the
contour plot should be created. For the 2D solver no surfaces are needed,
therefore pass an empty list when the options parameter is needed or
nothing.

##### `options`  
_Attributes: optional, default: `(list (list "filled-contours?" "yes") (list "node-values?" "yes") (list "global-range?" "no") (list "clip-to-range?" "no") (list "line-contours?" "no") (list "render-mesh?" "no") (list "n-contour" 20) (list "log-scale?" "no"))`_  
With this parameter the contour plot can be configured. The options are a
list of key value pairs. Each key value pair is a setting for the contour
plot. The key is a string that will be appended to the options base
command, which is the string "/display/set/contours/". The combined string
defines than a tui command and the value of the key value pair defines the
parameters for the tui command. So the following example for contour plot
`options`
```scheme
(list ; contour plot options (below are the default options)
  (list "filled-contours?" "yes") ; Filled
  (list "node-values?" "yes") ; Node Values
  (list "global-range?" "no") ; Global Range
  (list "clip-to-range?" "no") ; Clip to Range
  (list "line-contours?" "no") ; Draw Profiles
  (list "render-mesh?" "no") ; Draw Mesh
  (list "n-contour" 20) ; Levels
  (list "log-scale?" "no")
)
```
will expand to the following eight tui commands:  
1. `/display/set/contours/filled-contours? yes`
2. `/display/set/contours/node-values? yes`
3. `/display/set/contours/global-range? no`
4. `/display/set/contours/clip-to-range? no`
5. `/display/set/contours/line-contours? no`
6. `/display/set/contours/render-mesh? no`
7. `/display/set/contours/n-contour 20`
8. `/display/set/contours/log-scale? no`

##### `minimum`  
_Attributes: optional, default: `","`_  
The minimum value for the contour range.
Must be a number or the string `","`, which means auto range and is the
default setting.

##### `maximum`  
_Attributes: optional, default: `","`_  
The maximum value for the contour range.
Must be a number or the string `","`, which means auto range and is the
default setting.



