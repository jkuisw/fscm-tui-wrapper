:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: light-management.scm
Provides commands to handle lights in fluent.

:arrow_right: [Go to source code of this file.](/src/light-management.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: switch-light](#switch-light) | Turns all lights on or off.  > **Note:** The tui command `/display/set/light... |
| [:page_facing_up: switch-headlight](#switch-headlight) | Turns the headlight on or off.  Tui commands used:  + `/display/set/lights/h... |

## Procedure Documentation

### switch-light

#### Syntax
```scheme
(switch-light on [light-id] [re-render])
```

:arrow_right: [Go to source code of this procedure.](/src/light-management.scm#L27)

#### Export Name
`<import-name>/switch-light`

#### Description
Turns all lights on or off.  
> **Note:** The tui command `/display/set/lights/lights-on?` to switch the
> light on or off does not work (fluent 16.2).

Tui command used:
+ `/display/re-render` - re-render windows after switching light

#### Parameters
##### `on`  
If `#t` is passed, then all lights will be turned on, otherwise all
will be turned off.

##### `light-id`  
_Attributes: optional, default: `-1`_  
The id of the light which should be
switched. If `-1` is passed, then all lights will be switched. The light
id counts the lights starting by zero. The maximum light id is saved in
the `*cx-max-light*` variable. Therefore the light id cannot be greater
then `*cx-max-light*` and must be an integer greater than -2.

##### `re-render`  
_Attributes: optional, default: `#t`_  
When `#t`, then after switching the
light all windows will be rerendered. When `#f` is passed, rerendering
will be skipped.



-------------------------------------------------
### switch-headlight

#### Syntax
```scheme
(switch-headlight on [re-render])
```

:arrow_right: [Go to source code of this procedure.](/src/light-management.scm#L127)

#### Export Name
`<import-name>/switch-headlight`

#### Description
Turns the headlight on or off.  
Tui commands used:  
+ `/display/set/lights/headlight-on?` - turn headlight on or off
+ `/display/re-render` - re-render windows after switching headlight

#### Parameters
##### `on`  
If `#t` is passed, then the headlight will be turned on, otherwise it
will be turned off.

##### `re-render`  
_Attributes: optional, default: `#t`_  
When `#t`, then after switching the
headlight all windows will be rerendered. When `#f` is passed, rerendering
will be skipped.



