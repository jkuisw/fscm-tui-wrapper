:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: reports.scm
Implementes wrapper procedures for fluent text user interface (tui) report
commands.

:arrow_right: [Go to source code of this file.](/src/reports.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: wall-moments-report](#wall-moments-report) | Creates the wall moment report. Tui command used: `/report/forces/wall-mome... |
| [:page_facing_up: wall-forces-report](#wall-forces-report) | Creates the wall forces report. Tui command used: `/report/forces/wall-forc... |
| [:page_facing_up: mesh-size-report](#mesh-size-report) | Creates the mesh size report. Tui command used: `/mesh/size-info` |
| [:page_facing_up: mass-flow-report](#mass-flow-report) | Calculates the mass flow report for the given zones. Tui command used: `/re... |
| [:page_facing_up: settings-summary-report](#settings-summary-report) | Generates a summary report. Tui command used: `/report/summary` |
| [:page_facing_up: report-surf-aavg](#report-surf-aavg) | Generates an area weighted average surface integral report for the given pro... |

## Procedure Documentation

### wall-moments-report

#### Syntax
```scheme
(wall-moments-report zones moment-center moment-axis [all-wall-zones] [output-file])
```

:arrow_right: [Go to source code of this procedure.](/src/reports.scm#L27)

#### Export Name
`<import-name>/wall-moments-report`

#### Description
Creates the wall moment report.  
Tui command used: `/report/forces/wall-moments`

#### Parameters
##### `zones`  
The list of zones for which the momemnt report should be created. 
This must be a list of symbols and each symbol must be a valid zone name.

##### `moment-center`  
A vector which defines the moment center. The vector must 
be passed as a list of three numbers: 
`(list <x-coordinate> <y-coordinate> <z-coordinate>)`

##### `moment-axis`  
A vector which defines the moment axis. he vector must be 
passed as a list of three numbers: 
`(list <x-coordinate> <y-coordinate> <z-coordinate>)`

##### `all-wall-zones`  
_Attributes: optional, default: `#f`_  
When `#t` (true) passed, then all
wall zones will be used for the moment calculation.

##### `output-file`  
_Attributes: optional, default: `""`_  
When the moment report should be 
written to a file, than a valid file path must be passed. Existing files
will be overwritten.



-------------------------------------------------
### wall-forces-report

#### Syntax
```scheme
(wall-forces-report zones direction [all-wall-zones] [output-file])
```

:arrow_right: [Go to source code of this procedure.](/src/reports.scm#L113)

#### Export Name
`<import-name>/wall-forces-report`

#### Description
Creates the wall forces report.  
Tui command used: `/report/forces/wall-forces`

#### Parameters
##### `zones`  
The list of zones for which the force report should be created. 
This must be a list of symbols and each symbol must be a valid zone name.

##### `direction`  
A vector which defines the force direction vector. The vector
must be passed as a list of three numbers: 
`(list <x-coordinate> <y-coordinate> <z-coordinate>)`

##### `all-wall-zones`  
_Attributes: optional, default: `#f`_  
When `#t` (true) passed, then all
wall zones will be used for the force calculation.

##### `output-file`  
_Attributes: optional, default: `""`_  
When the force report should be 
written to a file, than a valid file path must be passed. Existing files
will be overwritten.



-------------------------------------------------
### mesh-size-report

#### Syntax
```scheme
(mesh-size-report)
```

:arrow_right: [Go to source code of this procedure.](/src/reports.scm#L181)

#### Export Name
`<import-name>/mesh-size-report`

#### Description
Creates the mesh size report.  
Tui command used: `/mesh/size-info`


-------------------------------------------------
### mass-flow-report

#### Syntax
```scheme
(mass-flow-report zones [all-boundary-zones] [output-file])
```

:arrow_right: [Go to source code of this procedure.](/src/reports.scm#L198)

#### Export Name
`<import-name>/mass-flow-report`

#### Description
Calculates the mass flow report for the given zones.  
Tui command used: `/report/fluxes/mass-flow`

#### Parameters
##### `zones`  
The list of zones for which the mass flow should be calculated. This
must be a list of symbols and each symbol must be a valid zone name.

##### `all-boundary-zones`  
_Attributes: optional, default: `#f`_  
When #t is passed, then all 
boundary zones will included in the calculation.

##### `output-file`  
_Attributes: optional, default: `""`_  
When the mass flow report should be 
written to a file, than a valid file path must be passed. Existing files
will be overwritten.



-------------------------------------------------
### settings-summary-report

#### Syntax
```scheme
(settings-summary-report [output-file])
```

:arrow_right: [Go to source code of this procedure.](/src/reports.scm#L258)

#### Export Name
`<import-name>/settings-summary-report`

#### Description
Generates a summary report.  
Tui command used: `/report/summary`

#### Parameters
##### `output-file`  
_Attributes: optional, default: `""`_  
When the summary report should be 
written to a file, than a valid file path must be passed. Existing files
will be overwritten.



-------------------------------------------------
### report-surf-aavg

#### Syntax
```scheme
(report-surf-aavg surfaces aavg-of)
```

:arrow_right: [Go to source code of this procedure.](/src/reports.scm#L304)

#### Export Name
`<import-name>/report-surf-aavg`

#### Description
Generates an area weighted average surface integral report for the given 
property.  
Tui command used: `/report/surface-integrals/area-weighted-avg`

#### Parameters
##### `surfaces`  
A list of surface names, for which the report should be created.

##### `aavg-of`  
The property for which the report should be generated, for 
example `"pressure"` or `"velocity-magnitude"`. For a full list of 
possible properties, execute the tui command 
`/report/surface-integrals/area-weighted-avg`, then when asked 
`area-weighted-avg of>` press enter and you get a list of possible 
properties.



