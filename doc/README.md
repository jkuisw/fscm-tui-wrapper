# Module: fscm-tui-wrapper
This is the overview of the module documentation. In this file you can find a list with all source files of the module, a list of all exported procedures of the module and a list of all module private procedures. The name of each list entry links to the specific documentation of the source file, exported procedure or private procedure.  

## Source Files
A list of all source files of this module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: light-management](/doc/light-management.md) | Provides commands to handle lights in fluent. |
| [:page_facing_up: reports](/doc/reports.md) | Implementes wrapper procedures for fluent text user interface (tui) report\n... |
| [:page_facing_up: tui-wrapper](/doc/tui-wrapper.md) | Implementes wrapper procedures for fluent text user interface (tui) commands... |

## Exported Procedures
A list of all exported procedures of this module. The procedure names are the internal procedure names. Normally this is the same as the exported name, prepended with the import name, but thy can differ. Look into the detailed procedure documentation to get the export name of the procedure. Exported procedures are imported into a source file with the `import` procedure (see the fscm-module-manager module for further details), which takes the import name as the first parameter. Therefore every exported procedure of a module will be imported according to the following syntax:  
`<import name>/<export name of the procedure>`.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: switch-light](/doc/light-management.md#switch-light) | Turns all lights on or off.  > **Note:** The tui command `/display/set/light... |
| [:page_facing_up: switch-headlight](/doc/light-management.md#switch-headlight) | Turns the headlight on or off.  Tui commands used:  + `/display/set/lights/h... |
| [:page_facing_up: wall-moments-report](/doc/reports.md#wall-moments-report) | Creates the wall moment report.  Tui command used: `/report/forces/wall-mome... |
| [:page_facing_up: wall-forces-report](/doc/reports.md#wall-forces-report) | Creates the wall forces report.  Tui command used: `/report/forces/wall-forc... |
| [:page_facing_up: mesh-size-report](/doc/reports.md#mesh-size-report) | Creates the mesh size report.  Tui command used: `/mesh/size-info` |
| [:page_facing_up: mass-flow-report](/doc/reports.md#mass-flow-report) | Calculates the mass flow report for the given zones.  Tui command used: `/re... |
| [:page_facing_up: settings-summary-report](/doc/reports.md#settings-summary-report) | Generates a summary report.  Tui command used: `/report/summary` |
| [:page_facing_up: report-surf-aavg](/doc/reports.md#report-surf-aavg) | Generates an area weighted average surface integral report for the given pro... |
| [:page_facing_up: string-list->tui-list](/doc/tui-wrapper.md#string-list-tui-list) | Converts a scheme list of strings (only strings!) to a fluent tui list, whic... |
| [:page_facing_up: execute-udf-function](/doc/tui-wrapper.md#execute-udf-function) | Executes a udf function which where declared by the "DEFINE_ON_DEMAND(...)"m... |
| [:page_facing_up: define-custom-field-function](/doc/tui-wrapper.md#define-custom-field-function) | Define a custom field function. If the force parameter is `#t`, than it will... |
| [:page_facing_up: display-surface-mesh](/doc/tui-wrapper.md#display-surface-mesh) | Displays the surface mesh for the given surfaces.  Tui commands used:  + `/d... |
| [:page_facing_up: set-udm-size](/doc/tui-wrapper.md#set-udm-size) | Set the size of the user defined memory.  Tui command used: `/define/user-de... |
| [:page_facing_up: read-case-file](/doc/tui-wrapper.md#read-case-file) | Reads the given case file (*.cas).  Tui command used: `/file/read-case` |
| [:page_facing_up: read-case-and-data](/doc/tui-wrapper.md#read-case-and-data) | Reads the data file (\*.dat) and the corresponding case file (\*.cas).  Tui ... |
| [:page_facing_up: create-iso-surface](/doc/tui-wrapper.md#create-iso-surface) | Creates a new iso surface. if an iso surface with the given name alreadyexis... |
| [:page_facing_up: xy-plot-to-file](/doc/tui-wrapper.md#xy-plot-to-file) | Writes the x-y plot values to the given file.  Tui command used: `/plot/plot... |
| [:page_facing_up: read-views](/doc/tui-wrapper.md#read-views) | Reads saved fluent views from a file (*.vw) and imports them.  Tui command u... |
| [:page_facing_up: save-picture](/doc/tui-wrapper.md#save-picture) | Procedure for saving pictures of the active window and the current view.  Tu... |
| [:page_facing_up: contour-plot](/doc/tui-wrapper.md#contour-plot) | Generates a contour plot of the given type.  Tui commands used:  + `/display... |

## Private Procedures
A list of all private procedures of this module. This procedures are not accessible from outside the module.  

  
The module does not have any private procedures.  
  

