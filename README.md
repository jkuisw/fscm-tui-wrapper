TUI Wrapper Module
==================

The `fscm-tui-wrapper` module provides wrapper procedures for fluent text user
interface (tui) commands. The wrappers document the parameters of the tui
commands and facilitates the use in scheme scripts. Furthermore some wrappers
are build of multiple tui commands to make some operations easier. Below the
installation and basic usage of this module is described, if you are looking for
the API documentation [click here](doc/README.md) (located in the `doc` folder).

If you'd like to contribute, please read the 
[contribution guide](CONTRIBUTING.md) first.

Installation
------------
Before you can install the TUI wrapper module you have to properly setup the
module manager. Follow the 
[Fluent Scheme Module Manager](https://gitlab.com/jkuisw/fscm-module-manager)
guide to set it up.

Now you need to clone the module repository to your computer. Choose a proper
location where you want to clone it, preferably the same directory where the
module manager repository is located (e.g.: `/home/<user name>/jkuisw`). Go into
that directory and clone the repository, as follows:

```shell
cd "/home/<user name>/jkuisw"
git clone --recurse-submodules git@gitlab.com:jkuisw/fscm-tui-wrapper.git
```

After cloning the repository ensure that the `fscm-tui-wrapper` module is 
registered to the module manager, as described in the 
[module manager setup](https://gitlab.com/jkuisw/fscm-module-manager#register-modules) 
(`add-module-paths` procedure).

Usage
-----
To use the TUI wrapper module you have to import it with a proper import name.
After that you can use the exported procedures of the module:

```scheme
; Import the TUI wrapper module.
(import "tui" "fscm-tui-wrapper")
```

The full documentation for all procedures exported by the TUI wrapper module can
be found in the [API documentation](doc/README.md). Below are only some examples
presented.  

### `define-custom-field-function` - Procedure
As the name of the procedure indicates, you can define a custom field function
with this command, as you can see in the following example:

```scheme
(tui/define-custom-field-function "vmagn-norm" "velocity_magnitude / 3.24")
```

### Save Picture of a Contour Plot
A more comprehensive example is to save a picture of a contour plot: 

```scheme
; Contour plot options.
(define contour-plot-opts (list
  (list "filled-contours?" "yes") ; Filled
  (list "node-values?" "yes") ; Node Values
  (list "global-range?" "no") ; Global Range
  (list "clip-to-range?" "no") ; Clip to Range
  (list "line-contours?" "no") ; Draw Profiles
  (list "render-mesh?" "no") ; Draw Mesh
  (list "n-contour" 20) ; Levels
  (list "log-scale?" "no")
))

; Picture driver options.
(define pic-driver-opts (list
  (list "x-resolution" 1920)
  (list "y-resolution" 1080)
))

; The content printer procedure which creates the contour plot.
(define (content-printer)
  ; Here we create our contour plot.
  (tui/contour-plot 
    "velocity-magnitude" ; type of contour plot
    (list 'iso-surf-z=0) ; surfaces for the contour plot
    contour-plot-opts ; contour plot options
    0 ; range minimum value
    4.6e+1 ; range maximum value
  )
)

(tui/save-picture
  "/path/to/the/picture.png" ; filepath of picture
  20 ; window number
  "name-of-view" ; view name
  content-printer ; content printer procedure
  "png" ; picture dirver (png is the default picture driver)
  pic-driver-opts ; picture driver options
)

```

In the example above, the `save-picture` procedure will do the following tasks:
1. Set the picture driver to "png"
2. Configure the picture driver options.
3. Set the window 20 as active (and create it if not existing).
4. Load the view "name-of-view".
5. Execute the content printer procedure, which creates the contour plot by
   calling the `contour-plot` procedure.
6. And finally save a picture of the contour plot to the given filepath.
