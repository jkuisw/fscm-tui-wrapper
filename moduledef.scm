;###############################################################################
; The module definition, this procedure must have exactly the name defined
; below and will be called by the module manager with one parameter, the
; absolute path to the module.
;
; Parameters:
;   module-path - The absolute path to the module.
;
; Returns: The module definition key value map (list).
(define (define-module module-path) (let ( (module-def '()) )
  (load (format #f "~a/module-version.scm" module-path))
  (set! module-def (list
    ; Module version according to semantic versioning, see: http://semver.org
    ; [mandatory]
    (list 'version module.fscm-tui-wrapper.version)

    ; List of supported platforms by the module, valid platforms are (symbols
    ; not strings!): 'linux, 'windows
    ; [mandatory]
    (list 'supported-platforms (list 'linux))

    ; List of modules this module depends on. A list entry is a list with three
    ; entries (the second and third are optional):
    ;   (1) The module name, which is the name of the directory containing the
    ;       module definitin file ("moduledef.scm").
    ;   (2) The version range for the module which defines what module versions
    ;       are valid. If ommited, than the newest available version of the
    ;       module will be used.
    ;   (3) The path to the module, relative to this file. If omitted, than the
    ;       path to the module must be added otherwise (or another definition
    ;       file has already or will be adding the path to the module).
    ; [optional]
    (list 'dependencies (list
      (list "fscm-utils" ">=0.5.0" "submodules/fscm-utils")
    ))

    ; A list of source directories, relative to this file. By default the module
    ; manager will only search for *.scm files in the module directory (which
    ; must be the directory of this file). If there are source (*.scm) files in
    ; subdirectories, you have to add them.
    ; NOTE: The module manager will not serach for source files above the module
    ; directory, even if you add them to the source directories!
    ; [optional]
    (list 'src-dirs (list "src"))

    ; The module manager version the module supports, must be a valid version
    ; range string. The module will only accept module managers which match
    ; with the version range defined.
    ; [mandatory]
    (list 'module-manager-version ">=1.2.0 <2.0.0")

    ; A list of objects to inject into the module environment. Every module is
    ; loaded into it's own environment. In this list you can define objects that
    ; will be loaded into the modules environment on creation of it (this is
    ; when you import the module the first time). An object can be any valid
    ; Scheme object, such as procedures or variables. This can be useful to
    ; inject custom variables (e.g.: settings, ...) or to inject procedures
    ; which are not defined in any other module and are not defined in the
    ; "user-initial-environment" environment.
    ; [optional]
    (list 'objects-to-inject '())
  ))

  ; Return the module definition.
  module-def
))
