#!/bin/bash

newVersion=$1
skipCi=${2-"no"}
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")

# shellcheck source=./config.sh
source "$scriptDir/config.sh"

# For a description of the parameters for the createVersion.sh command, see the
# help by calling "createVersion.sh -h".
if [[ $skipCi = "yes" ]]; then
  "${scriptPath:?}"/createVersion.sh \
    -r "${repositoryDirectory:?}" \
    -b "${productionBranch:?}" \
    -s \
    "$newVersion"
else
  "${scriptPath:?}"/createVersion.sh \
    -r "${repositoryDirectory:?}" \
    -b "${productionBranch:?}" \
    "$newVersion"
fi
