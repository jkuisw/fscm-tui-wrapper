#!/bin/bash

skipCi=${1-"no"}
tag=${2-""}
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")

# shellcheck source=./config.sh
source "$scriptDir/config.sh"

# For a description of the parameters for the packageFscmModule.sh command, see
# the help by calling "packageFscmModule.sh -h".
if [[ $skipCi = "yes" ]]; then
  if [[ -n $tag ]]; then
    "${scriptPath:?}"/packageFscmModule.sh -s -t "$tag" -r "${repositoryDirectory:?}"
  else
    "${scriptPath:?}"/packageFscmModule.sh -s -r "${repositoryDirectory:?}"
  fi
else
  if [[ -n $tag ]]; then
    "${scriptPath:?}"/packageFscmModule.sh -t "$tag" -r "${repositoryDirectory:?}"
  else
    "${scriptPath:?}"/packageFscmModule.sh -r "${repositoryDirectory:?}"
  fi
fi
