;*******************************************************************************
; Implementes wrapper procedures for fluent text user interface (tui) commands.

;*******************************************************************************
; Dependencies
(import "utils" "fscm-utils" "1.*.*")

;*******************************************************************************
; Converts a scheme list of strings (only strings!) to a fluent tui list, which
; can be used for tui commands a s input.  
; Example lists:  
; + scheme list: `(list "str1" "str2" "str3")`
; + tui list: `str1 str2 str3 '()`
;
; Parameters:
;   string-list - The valid scheme list of strings that should be converted to
;     the fluent tui list.
;
; Returns: The fluent tui list.
(define (string-list->tui-list string-list)
  (if (null? string-list)
    "'()"
    (string-append (utils/string-join string-list " ") " '()")
  )
)

;*******************************************************************************
; Executes a udf function which where declared by the "DEFINE_ON_DEMAND(...)"
; macro.  
; Tui command used: `/define/user-defined/execute-on-demand`
;
; Parameters:
;   libname - The name of the library of the udf function to execute,
;     e.g.: "libudf"
;   funcname - The name of the function to execute, this is the parameter of
;     the "DEFINE_ON_DEMAND(...)" macro.  
;     Example: `DEFINE_ON_DEMAND(calc_efficiency) { ... }` -> "calc_efficiency"
(define (execute-udf-function libname funcname) (let (
    (udf-string (string-append funcname "::" libname))
    (tuicmd "/define/user-defined/execute-on-demand")
  )
  (ti-menu-load-string (format #f "~a \"~a\"" tuicmd udf-string))
  (newline)
))

;*******************************************************************************
; Set the size of the user defined memory.  
; Tui command used: `/define/user-defined/user-defined-memory`
;
; Parameters:
;   size - The size of the user defined memory to set.
(define (set-udm-size size) (let (
    (tuicmd "/define/user-defined/user-defined-memory")
  )
  ; Check if parameter is valid.
  (if (not (number? size)) (error "Invalid parameter!" size))

  ; Execute tui command.
  (ti-menu-load-string (format #f "~a ~d" tuicmd size))
  (newline)
))

;*******************************************************************************
; Reads the given case file (*.cas).  
; Tui command used: `/file/read-case`
;
; Parameters:
;   case-file - The file path to the case file (directory and file name).
(define (read-case-file case-file) (let (
    (tuicmd "/file/read-case")
  )
  ; Check if parameter is valid.
  (if (and (not (string? case-file)) (not (symbol? case-file)))
    (error "Invalid parameter!" case-file)
  )

  ; Execute the tui command.
  ; "ok" is to force the read when there is already a case loaded and not saved.
  (ti-menu-load-string (format #f "~a \"~a\" ok" tuicmd case-file))
  (newline)
))

;*******************************************************************************
; Reads the data file (\*.dat) and the corresponding case file (\*.cas).  
; Tui command used: `/file/read-case-data`
;
; Parameters:
;   dat-file - The file path to the data file (directory and file name).
(define (read-case-and-data dat-file) (let (
    (tuicmd "/file/read-case-data")
  )
  ; Check if parameter is valid.
  (if (and (not (string? dat-file)) (not (symbol? dat-file)))
    (error "Invalid parameter!" dat-file)
  )

  ; Execute the tui command.
  ; "ok" is to force the read when there is already a case loaded and not saved.
  (ti-menu-load-string (format #f "~a \"~a\" ok" tuicmd dat-file))
  (newline)
))

;*******************************************************************************
; Creates a new iso surface. if an iso surface with the given name already
; exists, than it will be deleted.  
; Tui commands used: `/surface/delete-surface` and `/surface/iso-surface`  
;  
; Example:  
; ```scheme
; (<import-name>/create-iso-surface
;   "x-coordinate" ; iso value type
;   surface-name ; iso-surface name
;   (list 0.03851) ; iso values list
;   (list 'laufrad_outer_interface) ; from surfaces list
; )
; ```
;
; Parameters:
;   type - The type of the iso value, for example `"x-coordinate"` or
;     `"radial-coordinate"`, passed as string. For a full list of possible
;     types, execute the tui command `/surface/iso-surface`, then when asked
;     `iso-surface of>` press enter and you get a list of possible types.
;   name - The name of the new surface.
;   iso-values - A list of iso values, the number of needed iso values depends
;     on the type.
;   from-surfaces - [optional, default: '()] A list of surface names, from which
;     the iso surface combined with the iso values will be created.
;   from-zones - [optional, default: '()] A list of zone names, which must be
;     symbols, from which the iso surface combined with the iso values will be
;     created.
(define (create-iso-surface type name iso-values . args) (let (
    (tui-string "")
    (delete-cmd "/surface/delete-surface")
    (create-cmd "/surface/iso-surface")
    (from-surfaces '())
    (from-zones '())
    (found #f)
  )
  ; Check if parameters are valid.
  (if (and (not (string? type)) (not (symbol? type)))
    (error "Invalid parameter!" type)
  )
  (if (and (not (string? name)) (not (symbol? name)))
    (error "Invalid parameter!" name)
  )
  (if (not (pair? iso-values)) (error "Invalid parameter!" iso-values))

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; from-surfaces parameter passed.
    (set! from-surfaces (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (pair? from-surfaces)) (error "Invalid parameter!" from-surfaces))
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; from-zones parameter passed.
    (set! from-zones (list-ref args 1))
    ; Check if parameter is valid.
    (if (not (pair? from-zones)) (error "Invalid parameter!" from-zones))
  ))

  ; Check if surface already exists and if so, delete it.
  (set! found (let loop ((remaining-surfaces (inquire-surface-names)))
    (cond
      ; Check if reached end of list.
      ((null? remaining-surfaces) #f)
      ; Check if found surface name.
      ((string=? (utils/to-string (car remaining-surfaces)) name) #t)
      ; Go to the next list element.
      (else (loop (cdr remaining-surfaces)))
    )
  ))

  (if found (begin
    ; Execute the tui command delete surface.
    (ti-menu-load-string (string-append delete-cmd " " name))
    (newline)
  ))

  ; Build the tui command string.
  (set! tui-string (string-append
    create-cmd "/" type " " name " "
    (string-list->tui-list (utils/object-list->string-list from-surfaces)) " "
    (string-list->tui-list (utils/object-list->string-list from-zones)) " "
    (string-list->tui-list (utils/object-list->string-list iso-values))
  ))

  ; Execute the tui command.
  (ti-menu-load-string tui-string)
  (newline)
))

;*******************************************************************************
; Writes the x-y plot values to the given file.  
; Tui command used: `/plot/plot`  
;  
; Example:  
; ```scheme
; (<import-name>/xy-plot-to-file
;   "/path/to/file.xy" ; file path
;   "angular-coordinate" ; x cell function
;   "pressure" ; y cell function
;   (list surface-name) ; list of sufaces
; )
; ```
;
; Parameters:
;   filepath - The path to the file where the x-y plot values should be written
;     to.
;   x-function - The x cell function for the plot operation, for example
;     `"x-coordinate"` or `"radial-coordinate"`. For a full list of possible
;     cell functions, execute the tui command `/plot/plot`, then when asked
;     `cell function>` press enter and you get a list of possible cell
;     functions.
;   y-function - The y cell function for the plot operation, for example
;     `"pressure"` or `"velocity-mag"`. For a full list of possible cell
;     functions, execute the tui command `/plot/plot`, then when asked
;     `cell function>` press enter and you get a list of possible cell
;     functions.
;   surfaces - A list of surface names for which the plot operation should
;     apply.
;   use-node-values - [optional, default: #t] When passed #t, then node values
;     will be used for the plot operation.
;   order-points - [optional, default: #t] When passed #t, then the values
;     written to the output file will be sorted by the coordinates fo the
;     points.
(define (xy-plot-to-file filepath x-function y-function surfaces . args) (let (
    (tui-string "")
    (tuicmd "/plot/plot")
    (use-node-values #t)
    (order-points #t)
  )
  ; Check if parameters are valid.
  (if (and (not (string? filepath)) (not (symbol? filepath)))
    (error "Invalid parameter!" filepath)
  )
  (if (and (not (string? x-function)) (not (symbol? x-function)))
    (error "Invalid parameter!" x-function)
  )
  (if (and (not (string? y-function)) (not (symbol? y-function)))
    (error "Invalid parameter!" y-function)
  )
  (if (not (pair? surfaces)) (error "Invalid parameter!" surfaces))

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; use-node-values parameter passed.
    (set! use-node-values (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (boolean? use-node-values))
      (error "Invalid parameter!" use-node-values)
    )
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; order-points parameter passed.
    (set! order-points (list-ref args 1))
    ; Check if parameter is valid.
    (if (not (boolean? order-points)) (error "Invalid parameter!" order-points))
  ))

  ; Build the tui command string.
  ; The command parameters in the order the "plot" command will ask for them:
  ;   node values? (yes|no)
  ;   filename
  ;   OK to overwrite? (ok|cancel)
  ;   order points? (yes|no)
  ;   Y Axis direction vector (yes|no)
  ;   Y Axis curve length (yes|no)
  ;   Y Axis cell function
  ;   X Axis direction vector (yes|no)
  ;   X Axis curve length (yes|no)
  ;   X Axis cell function
  ;   surface id/name list
  (set! tui-string (string-append
    tuicmd " "
    (if use-node-values "yes " "no ")
    "\"" filepath "\" "
    ; If file already exists, overwrite it.
    (if (file-exists? filepath) "ok " "")
    (if order-points "yes " "no ")
    "no " ; No y axis direction vector.
    "no " ; No y axis curve length.
    y-function " "
    "no " ; No x axis direction vector.
    "no " ; No x axis curve length.
    x-function " "
    (string-list->tui-list (utils/object-list->string-list surfaces))
  ))

  ; Execute the tui command.
  (ti-menu-load-string tui-string)
  (newline)
))

;*******************************************************************************
; Reads saved fluent views from a file (*.vw) and imports them.  
; Tui command used: `/views/read-views`
;
; Parameters:
;   filepath - The file path to the views file (file extension *.vw).
(define (read-views filepath) (let (
    (tuicmd "/views/read-views")
  )
  ; Check if parameter is valid.
  (if (and (not (string? filepath)) (not (symbol? filepath)))
    (error "Invalid parameter!" filepath)
  )
  (if (not (file-exists? filepath)) (error "File does not exist!" filepath))

  ; Execute the tui command.
  (ti-menu-load-string
    (format #f "~a \"~a\" ok" tuicmd (utils/to-string filepath))
  )
  (newline)
))

;*******************************************************************************
; Procedure for saving pictures of the active window and the current view.  
; Tui commands used:  
; + `/display/open-window` - to open a window
; + `/display/set-window` - for setting the active window
; + `/display/views/restore-view` - for restoring a saved view
; + `/display/set/picture` - base command for picture settings, see
;   `picture-options` parameter
; + `/display/save-picture` - command to save the picture  
;  
; Example:  
; ```scheme
; (<import-name>/save-picture
;   "/path/to/the/picture.png" ; filepath of picture
;   20 ; window number
;   "name-of-view" ; view name
;   (lambda () (begin ; content printer procedure
;     ; print some content to the active window, e.g.: a contour plot
;   ))
;   "png" ; picture dirver (png is the default picture driver)
;   (list ; picture driver options (below are the default options)
;     (list "x-resolution" 1920)
;     (list "y-resolution" 1080)
;   )
; )
; ```
;
; Parameters:
;   filepath - File path to where the picture should be saved (directory and
;     file name).
;   window - The number of the window from which the picture should be taken.
;   view - The name of the view for the picture. Must be an existing view,
;     because the view will be restored in the active window befor taking the
;     picture.
;   content-printer - The content-printer is a procedure that will be called,
;     after setting the active window and restoring the view and before taking
;     the picture. It is responsible for printing content to the window, such as
;     a contour plot of something, from which a picture should be saved.
;   picture-driver - [optional, default: "png"] The picture driver to use. You
;     can see the possible picture drivers when executing the tui command
;     `/display/set/picture/driver`.
;   picture-options - [optional, default: (list (list "x-resolution" 960) (list "y-resolution" 720))]
;     With this parameter the picture dirver can be configured. The display
;     options are a list of key value pairs. Each key value pair is a setting
;     for the picture driver. The key is a string that will be appended to the
;     picture base command, which is the string "/display/set/picture/". The
;     combined string defines than a tui command and the value of the key value
;     pair defines the parameters for the tui command. So the following example
;     for `picture-options`  
;     ```scheme
;     (list ; picture driver options (below are the default options)
;       (list "x-resolution" 1920)
;       (list "y-resolution" 1080)
;     )
;     ```
;     will expand to the following two tui commands:  
;     1. `/display/set/picture/x-resolution 1920`
;     2. `/display/set/picture/y-resolution 1080`
(define (save-picture filepath window view content-printer . args) (let (
    (open-window-cmd "/display/open-window")
    (set-window-cmd "/display/set-window")
    (restore-view-cmd "/display/views/restore-view")
    (pic-base-cmd "/display/set/picture")
    (pic-save-cmd "/display/save-picture")
    (picture-driver "png")
    (picture-options (list
      (list "x-resolution" 960)
      (list "y-resolution" 720)
    ))
  )
  ; Check if parameters are valid.
  (if (and (not (string? filepath)) (not (symbol? filepath)))
    (error "Invalid parameter!" filepath)
  )
  (if (not (integer? window))
    (error "Invalid parameter!" window)
  )
  (if (and (not (string? view)) (not (symbol? view)))
    (error "Invalid parameter!" view)
  )
  (if (not (procedure? content-printer))
    (error "Invalid parameter!" content-printer)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; picture-driver parameter passed.
    (set! picture-driver (list-ref args 0))
    ; Check if parameter is valid.
    (if (and (not (string? picture-driver)) (not (symbol? picture-driver)))
      (error "Invalid parameter!" picture-driver)
    )
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; picture-options parameter passed.
    (set! picture-options (list-ref args 1))
    ; Check if parameter is valid.
    (if (not (pair? picture-options))
      (error "Invalid parameter!" picture-options)
    )
  ))

  ; Open and set the active window and restore the view.
  (ti-menu-load-string (format #f "~a ~d" open-window-cmd window)) (newline)
  (ti-menu-load-string (format #f "~a ~d" set-window-cmd window)) (newline)
  (ti-menu-load-string (format #f "~a ~a" restore-view-cmd view)) (newline)

  ; Execute the content printer procedure.
  (content-printer)

  ; Set the picture driver and configure it.
  (ti-menu-load-string (format #f "~a/driver/~a" pic-base-cmd picture-driver))
  (newline)
  (for-each (lambda (option) (let (
      (key (list-ref option 0))
      (value (list-ref option 1))
    )
    (ti-menu-load-string
      (format #f "~a/~a ~a" pic-base-cmd
        (utils/to-string key) (utils/to-string value))
    )
    (newline)
  )) picture-options)

  ; Save the picture.
  (ti-menu-load-string (format #f "~a \"~a\"" pic-save-cmd filepath))
  (newline)
))

;*******************************************************************************
; Generates a contour plot of the given type.  
; Tui commands used:  
; + `/display/set/contours` - base command for contour plot settings, see
;   `options` parameter
; + `/display/set/contours/surfaces` - define surfaces for the contour plot
; + `/display/contour` - command for creating contour plot  
;  
; Example:  
; ```scheme
; (<import-name>/contour-plot
;   "velocity-magnitude" ; type of contour plot
;   (list 'iso-surf-z=0) ; surfaces for the contour plot
;   (list ; contour plot options (below are the default options)
;     (list "filled-contours?" "yes") ; Filled
;     (list "node-values?" "yes") ; Node Values
;     (list "global-range?" "no") ; Global Range
;     (list "clip-to-range?" "no") ; Clip to Range
;     (list "line-contours?" "no") ; Draw Profiles
;     (list "render-mesh?" "no") ; Draw Mesh
;     (list "n-contour" 20) ; Levels
;     (list "log-scale?" "no")
;   )
;   0 ; range minimum value
;   4.6e+1 ; range maximum value
; )
; ```
;
; Parameters:
;   contour-of - The type of the contour plot, for example `"pressure"` or
;     `"velocity-magnitude"`. For a full list of possible contour plot types,
;     execute the tui command `/display/contour`, then when asked
;     `contours of>` press enter and you get a list of possible contour plot
;     types.
;   surfaces - [optional, default: '()] A list of surface names, for which the
;     contour plot should be created. For the 2D solver no surfaces are needed,
;     therefore pass an empty list when the options parameter is needed or
;     nothing.
;   options - [optional, default: (list (list "filled-contours?" "yes") (list "node-values?" "yes") (list "global-range?" "no") (list "clip-to-range?" "no") (list "line-contours?" "no") (list "render-mesh?" "no") (list "n-contour" 20) (list "log-scale?" "no"))]
;     With this parameter the contour plot can be configured. The options are a
;     list of key value pairs. Each key value pair is a setting for the contour
;     plot. The key is a string that will be appended to the options base
;     command, which is the string "/display/set/contours/". The combined string
;     defines than a tui command and the value of the key value pair defines the
;     parameters for the tui command. So the following example for contour plot
;     `options`
;     ```scheme
;     (list ; contour plot options (below are the default options)
;       (list "filled-contours?" "yes") ; Filled
;       (list "node-values?" "yes") ; Node Values
;       (list "global-range?" "no") ; Global Range
;       (list "clip-to-range?" "no") ; Clip to Range
;       (list "line-contours?" "no") ; Draw Profiles
;       (list "render-mesh?" "no") ; Draw Mesh
;       (list "n-contour" 20) ; Levels
;       (list "log-scale?" "no")
;     )
;     ```
;     will expand to the following eight tui commands:  
;     1. `/display/set/contours/filled-contours? yes`
;     2. `/display/set/contours/node-values? yes`
;     3. `/display/set/contours/global-range? no`
;     4. `/display/set/contours/clip-to-range? no`
;     5. `/display/set/contours/line-contours? no`
;     6. `/display/set/contours/render-mesh? no`
;     7. `/display/set/contours/n-contour 20`
;     8. `/display/set/contours/log-scale? no`
;   minimum - [optional, default: ","] The minimum value for the contour range.
;     Must be a number or the string `","`, which means auto range and is the
;     default setting.
;   maximum - [optional, default: ","] The maximum value for the contour range.
;     Must be a number or the string `","`, which means auto range and is the
;     default setting.
(define (contour-plot contour-of . args) (letrec (
    (options-base-cmd "/display/set/contours")
    (set-surfaces-cmd "/display/set/contours/surfaces")
    (contour-cmd "/display/contour")
    (surfaces '())
    (options (list
      (list "filled-contours?" "yes") ; Filled
      (list "node-values?" "yes") ; Node Values
      (list "global-range?" "no") ; Global Range
      (list "clip-to-range?" "no") ; Clip to Range
      (list "line-contours?" "no") ; Draw Profiles
      (list "render-mesh?" "no") ; Draw Mesh
      (list "n-contour" 20) ; Levels
      (list "log-scale?" "no")
    ))
    (minimum ",") ; Auto Range
    (maximum ",") ; Auto Range
    (check-range-value (lambda (val)
      (if (string? val) (begin
        (if (not (string=? val ",")) (error "Invalid parameter \"minimum\" or \"maximum\"!" val))
      ) (begin ; else
        (if (not (number? val)) (error "Invalid parameter \"minimum\" or \"maximum\"!" val))
      ))
    ))
  )
  ; Check if parameters are valid.
  (if (and (not (string? contour-of)) (not (symbol? contour-of)))
    (error "Invalid parameter \"contour-of\"!" contour-of)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; surfaces parameter passed.
    (set! surfaces (list-ref args 0))
    ; Check if parameter is valid.
    (if (and (not (pair? surfaces)) (not (null? surfaces)))
      (error "Invalid parameter \"surfaces\"!" surfaces)
    )
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; options parameter passed.
    (set! options (list-ref args 1))
    ; Check if parameter is valid.
    (if (and (not (pair? options)) (not (null? options)))
      (error "Invalid parameter \"options\"!" options)
    )
  ))

  ; Parse optional arguments.
  (if (> (length args) 2) (begin
    ; minimum parameter passed.
    (set! minimum (list-ref args 2))
    ; Check if parameter is valid.
    (check-range-value minimum)
  ))

  ; Parse optional arguments.
  (if (> (length args) 3) (begin
    ; maximum parameter passed.
    (set! maximum (list-ref args 3))
    ; Check if parameter is valid.
    (check-range-value maximum)
  ))

  ; Configure the contour plot.
  (for-each (lambda (option) (let (
      (key (list-ref option 0))
      (value (list-ref option 1))
    )
    (ti-menu-load-string
      (format #f "~a/~a ~a" options-base-cmd
        (utils/to-string key) (utils/to-string value))
    )
    (newline)
  )) options)

  ; Only execute set surfaces command when surface list is not an empty one.
  (if (not (null? surfaces))
    (ti-menu-load-string (string-append
      set-surfaces-cmd " "
      (string-list->tui-list (utils/object-list->string-list surfaces))
    ))
    (newline)
  )

  ; Generate the contour plot.
  (ti-menu-load-string (string-append
    contour-cmd " "
    (utils/to-string contour-of) " "
    (utils/to-string minimum) " "
    (utils/to-string maximum)
  ))
  (newline)
))

;*******************************************************************************
; Define a custom field function. If the force parameter is `#t`, than it will
; be checked if the passed custom field function name already exists. When this
; is the case, than the existing custom field function will be deleted.  
; Tui commands used: `/define/custom-field-function/delete` and
; `/define/custom-field-function/define`
;
; Parameters:
;   name - The name for the custom field function, must be a string or symbol.
;   code - The code for the custom field function, must be a string or symbol.
;   force - [optional, default: #f] When passed `#t`, than existing custom field
;     function with the same name will be deleted before creation of the new
;     one.
(define (define-custom-field-function name code . args) (let (
    (force #f)
    (continue #t)
    (cffs (let loop ((remaining (inquire-cell-functions-sectioned)))
      (cond
        ; Check if reached end of list, if so, than the custom field functions
        ; entry could not be found.
        ((null? remaining) '())
        ; Check if the first list entry of remaining is the custom field
        ; functions entry. (general-car-cdr: car = 0, cdr = 1, from right to
        ; left, last is end of sequence)
        ((string=?
            (general-car-cdr remaining #b1010)
            "Custom Field Functions..."
          )
          ; Return the list of existing
          (map (lambda (cff) (car cff)) (general-car-cdr remaining #b1110))
        )
        ; There are remaining list entries and the custom field functions entry
        ; was not found yet. Therefore continue with the next list elements.
        (else (loop (cdr remaining)))
      )
    ))
  )
  ; Check parameters.
  (if (symbol? name) (set! name (utils/to-string name)))
  (if (not (string? name)) (error "Invalid parameter!" name))
  (if (symbol? code) (set! code (utils/to-string code)))
  (if (not (string? code)) (error "Invalid parameter!" code))

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; force parameter passed.
    (set! force (car args))
    ; Check if parameter is valid.
    (if (not (boolean? force)) (error "Invalid parameter!" force))
  ))

  ; Check if custom field function already exists.
  (if (member name cffs) (begin
    ; Custom field function exits. If force is true, delete it and continue. If
    ; force is false do not continue and return false.
    (if force (begin
      ; Execute tui command.
      (ti-menu-load-string (string-append
        "/define/custom-field-function/delete "
        (string-list->tui-list (list name))
      ))
      (newline)
    ) (begin ; else
      (set! continue #f)
    ))
  ))

  (if continue (begin
    ; Execute tui command.
    (ti-menu-load-string
      (format #f "/define/custom-field-function/define \"~a\" ~a" name code)
    )
    (newline)
  ) (begin ; else
    #f ; Return false.
  ))
))

;*******************************************************************************
; Displays the surface mesh for the given surfaces.  
; Tui commands used:  
; + `/display/set` - base command for configuration, see `options` parameter
; + `/display/surface-mesh` - command to display surface mesh  
;  
; Example:  
; ```scheme
; (<import-name>/display-surface-mesh
;   (list 'surface-name-1 'surface-name-2)
;   (list ; command options (below are the default options)
;     "filled-mesh? yes" ; Options: Faces (yes/no)
;     ; Options: Edges (yes/no), if edges yes - Edge Type:
;     ;  All edges? (yes/no), if All edges? no:
;     ;    Outline edges? (yes/no)
;     ;    Feature edges? (yes/no), if Feature edges? yes:
;     ;      Feature angle? (number)
;     "rendering-options/surface-edge-visibility yes yes"
;     "mesh-partitions? no" ; Options: Partitions (yes/no)
;     "element-shrink 0" ; Shrink Factor
;   )
; )
; ```
;
; Parameters:
;   surfaces - A list of surface names, for which the surface mesh should be
;     displayed.
;   options - [optional, default: (list "filled-mesh? yes" "rendering-options/surface-edge-visibility yes yes" "mesh-partitions? no" "element-shrink 0")]
;     With this parameter the command can be configured. The options are a list
;     of strings. Each string will be appended to the options base command,
;     which is the string "/display/set". The combined string defines than a tui
;     command that will be executed to configure the display surface mesh
;     command. So the following example for the command `options`  
;     ```scheme
;     (list ; command options (below are the default options)
;       "filled-mesh? yes" ; Options: Faces (yes/no)
;       ; Options: Edges (yes/no), if edges yes - Edge Type:
;       ;  All edges? (yes/no), if All edges? no:
;       ;    Outline edges? (yes/no)
;       ;    Feature edges? (yes/no), if Feature edges? yes:
;       ;      Feature angle? (number)
;       "rendering-options/surface-edge-visibility yes yes"
;       "mesh-partitions? no" ; Options: Partitions (yes/no)
;       "element-shrink 0" ; Shrink Factor
;     )
;     ```
;     will expand to the following four tui commands:  
;     1. `/display/set/filled-mesh? yes`
;     2. `/display/set/rendering-options/surface-edge-visibility yes yes`
;     3. `/display/set/mesh-partitions? no`
;     4. `/display/set/element-shrink 0`
(define (display-surface-mesh surfaces . args) (letrec (
    (options-base-cmd "/display/set")
    (display-cmd "/display/surface-mesh")
    (options (list
      "filled-mesh? yes" ; Options: Faces (yes/no)
      ; Options: Edges (yes/no), if edges yes - Edge Type:
      ;  All edges? (yes/no), if All edges? no:
      ;    Outline edges? (yes/no)
      ;    Feature edges? (yes/no), if Feature edges? yes:
      ;      Feature angle? (number)
      "rendering-options/surface-edge-visibility yes yes"
      "mesh-partitions? no" ; Options: Partitions (yes/no)
      "element-shrink 0" ; Shrink Factor
    ))
  )
  ; Check if parameter is valid.
  (if (not (pair? surfaces)) (error "Invalid parameter!" surfaces))

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; options parameter passed.
    (set! options (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (pair? options))
      (error "Invalid parameter!" options)
    )
  ))

  ; Configure the surface mesh plot.
  (for-each (lambda (option)
    (ti-menu-load-string (format #f "~a/~a" options-base-cmd option))
    (newline)
  ) options)

  ; Display the surface mesh.
  (ti-menu-load-string (string-append
    display-cmd " "
    (string-list->tui-list (map (lambda (surface)
      (if (not (symbol? surface))
        (set! surface (string->symbol (utils/to-string surface)))
      )
      (utils/to-string (surface-name->id surface))
    ) surfaces))
  ))
  (newline)
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'string-list->tui-list string-list->tui-list)
  (list 'execute-udf-function execute-udf-function)
  (list 'set-udm-size set-udm-size)
  (list 'read-case-file read-case-file)
  (list 'read-case-and-data read-case-and-data)
  (list 'create-iso-surface create-iso-surface)
  (list 'xy-plot-to-file xy-plot-to-file)
  (list 'read-views read-views)
  (list 'save-picture save-picture)
  (list 'contour-plot contour-plot)
  (list 'define-custom-field-function define-custom-field-function)
  (list 'display-surface-mesh display-surface-mesh)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
