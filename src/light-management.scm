;*******************************************************************************
; Provides commands to handle lights in fluent.

;*******************************************************************************
; Dependencies
(import "utils" "fscm-utils")

;*******************************************************************************
; Turns all lights on or off.  
; > **Note:** The tui command `/display/set/lights/lights-on?` to switch the
; > light on or off does not work (fluent 16.2).
;
; Tui command used:
; + `/display/re-render` - re-render windows after switching light
;
; Parameters:
;   on - If `#t` is passed, then all lights will be turned on, otherwise all
;     will be turned off.
;   light-id - [optional, default: -1] The id of the light which should be
;     switched. If `-1` is passed, then all lights will be switched. The light
;     id counts the lights starting by zero. The maximum light id is saved in
;     the `*cx-max-light*` variable. Therefore the light id cannot be greater
;     then `*cx-max-light*` and must be an integer greater than -2.
;   re-render - [optional, default: #t] When `#t`, then after switching the
;     light all windows will be rerendered. When `#f` is passed, rerendering
;     will be skipped.
(define (switch-light on . args) (let (
    (light-id -1)
    (re-render #t)
  )
  ; Check if parameter is valid.
  (if (not (boolean? on)) (error "Invalid parameter!" on))

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; light-id parameter passed.
    (set! light-id (car args))
    ; Check if parameter is valid.
    (if (not (integer? light-id)) (error "Invalid parameter!" light-id))
    (if (or (< light-id -1) (> light-id *cx-max-light*)) (error
      (format #f "Parameter \"light-id\" must be an integer >= ~d and <= ~d!"
        -1 *cx-max-light*)
      light-id
    ))
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; re-render parameter passed.
    (set! re-render (list-ref args 1))
    ; Check if parameter is valid.
    (if (not (boolean? re-render)) (error "Invalid parameter!" re-render))
  ))

  ; Loop through lights settings to switch lights.
  (let loop ( (remaining-lights (cxgetvar 'lights-list)) ) (let (
      (curr-light-settings '())
      (curr-light-id -1)
      (light-id-list '())
    )
    (if (not (null? remaining-lights)) (begin
      ; Get current light settings.
      (set! curr-light-settings (car remaining-lights))

      ; Calculate current light id.
      (set! light-id-list (utils/string-split (car curr-light-settings) #\-))
      (set! curr-light-id (string->number (list-ref
        light-id-list
        (- (length light-id-list) 1)
      )))
    ))

    (cond
      ; Check if reached end of list. If so, than all lights are updated.
      ((null? remaining-lights) #t)
      ; Only switch light when all lights should be switched (when
      ; light-id = -1 was passed) or if the current light has the requested
      ; light-id.
      ((or (= light-id -1) (= curr-light-id light-id)) (let (
          (is-curr-light-on (light->on? curr-light-settings))
        )
        (if (or
            ; Requested to turn light off (on = #f), check that current light is
            ; not already off.
            (and is-curr-light-on (not on))
            ; Requested to turn light on (on = #t), check that current light is
            ; not already on.
            (and (not is-curr-light-on) on)
          )
          ; Turn light off.
          (cx-set-light
            curr-light-id ; the light id
            on ; turn light off (#f) or on (#t)
            (light->rgb curr-light-settings) ; don't change light color
            (light->xyz curr-light-settings) ; don't change light direction
          )
        )

        ; Go to next light.
        (loop (cdr remaining-lights))
      ))
      ; Go to next light when light is already in the requested state.
      (else (loop (cdr remaining-lights)))
    )
  ))

  ; Check if schould re-render.
  (if re-render (begin
    ; Re-render windows.
    (ti-menu-load-string "/display/re-render")
    (newline)
  ))
))

;*******************************************************************************
; Turns the headlight on or off.  
; Tui commands used:  
; + `/display/set/lights/headlight-on?` - turn headlight on or off
; + `/display/re-render` - re-render windows after switching headlight  
;
; Parameters:
;   on - If `#t` is passed, then the headlight will be turned on, otherwise it
;     will be turned off.
;   re-render - [optional, default: #t] When `#t`, then after switching the
;     headlight all windows will be rerendered. When `#f` is passed, rerendering
;     will be skipped.
(define (switch-headlight on . args) (let (
    (re-render #t)
  )
  ; Check if parameter is valid.
  (if (not (boolean? on)) (error "Invalid parameter!" on))

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; re-render parameter passed.
    (set! re-render (car args))
    ; Check if parameter is valid.
    (if (not (boolean? re-render)) (error "Invalid parameter!" re-render))
  ))

  ; Execute tui command.
  (ti-menu-load-string (string-append
    "/display/set/lights/headlight-on? "
    (if on "yes" "no")
  ))
  (newline)

  ; Check if schould re-render.
  (if re-render (begin
    ; Re-render windows.
    (ti-menu-load-string "/display/re-render")
    (newline)
  ))
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'switch-light switch-light)
  (list 'switch-headlight switch-headlight)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
    
