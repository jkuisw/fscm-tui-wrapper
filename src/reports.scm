;*******************************************************************************
; Implementes wrapper procedures for fluent text user interface (tui) report
; commands.

;*******************************************************************************
; Dependencies
(import "utils" "fscm-utils" "1.*.*")

;*******************************************************************************
; Creates the wall moment report.  
; Tui command used: `/report/forces/wall-moments`
;
; Parameters:
;   zones - The list of zones for which the momemnt report should be created. 
;     This must be a list of symbols and each symbol must be a valid zone name.
;   moment-center - A vector which defines the moment center. The vector must 
;     be passed as a list of three numbers: 
;     `(list <x-coordinate> <y-coordinate> <z-coordinate>)`
;   moment-axis - A vector which defines the moment axis. he vector must be 
;     passed as a list of three numbers: 
;     `(list <x-coordinate> <y-coordinate> <z-coordinate>)`
;   all-wall-zones - [optional, default: #f] When `#t` (true) passed, then all
;     wall zones will be used for the moment calculation.
;   output-file - [optional, default: ""] When the moment report should be 
;     written to a file, than a valid file path must be passed. Existing files
;     will be overwritten.
(define (wall-moments-report zones moment-center moment-axis . args) (let (
    (tui-string "")
    (tuicmd "/report/forces/wall-moments")
    (all-wall-zones #f)
    (output-file "")
  )
  ; Check if parameters are valid.
  (if (not (pair? zones)) (error "Invalid parameter!" zones))
  (if (or (not (pair? moment-center)) (< (length moment-center) 3))
    (error "Invalid parameter!" moment-center)
  )
  (if (or (not (pair? moment-axis)) (< (length moment-axis) 3))
    (error "Invalid parameter!" moment-axis)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; all-wall-zones parameter passed.
    (set! all-wall-zones (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (boolean? all-wall-zones))
      (error "Invalid parameter!" all-wall-zones)
    )
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; output-file parameter passed.
    (set! output-file (list-ref args 1))
    ; Check if parameter is valid.
    (if (and (not (string? output-file)) (not (symbol? output-file)))
      (error "Invalid parameter!" output-file)
    )
  ))

  ; Build the tui command string.
  ; The command parameters in the order the "wall-moments" command will ask 
  ; for them:
  ;   all wall zones? (yes|no)
  ;   zones list
  ;   x-coordinate of moment center
  ;   y-coordinate of moment center
  ;   z-coordinate of moment center
  ;   x-component of moment axis
  ;   y-component of moment axis
  ;   z-component of moment axis
  ;   write to file? (yes|no)
  (set! tui-string (string-append
    tuicmd " "
    (if all-wall-zones
      "yes " ; use all wall zones for calculation
      (string-append "no " ; use only zones in list
        (string-list->tui-list (utils/object-list->string-list zones)) " ")
    )
    (utils/string-join (utils/object-list->string-list moment-center) " ") " "
    (utils/string-join (utils/object-list->string-list moment-axis) " ") " "
    (if (> (string-length output-file) 0)
      (string-append ; output file passed as parameter
        "yes \"" output-file "\""
        ; If output file exists, overwrite it.
        (if (file-exists? output-file) " no ok" "")
      )
      "no" ; no output file
    )
  ))

  ; Execute the tui command.
  (ti-menu-load-string tui-string)
  (newline)
))

;*******************************************************************************
; Creates the wall forces report.  
; Tui command used: `/report/forces/wall-forces`
;
; Parameters:
;   zones - The list of zones for which the force report should be created. 
;     This must be a list of symbols and each symbol must be a valid zone name.
;   direction - A vector which defines the force direction vector. The vector
;     must be passed as a list of three numbers: 
;     `(list <x-coordinate> <y-coordinate> <z-coordinate>)`
;   all-wall-zones - [optional, default: #f] When `#t` (true) passed, then all
;     wall zones will be used for the force calculation.
;   output-file - [optional, default: ""] When the force report should be 
;     written to a file, than a valid file path must be passed. Existing files
;     will be overwritten.
(define (wall-forces-report zones direction . args) (let (
    (tui-string "")
    (tuicmd "/report/forces/wall-forces")
    (all-wall-zones #f)
    (output-file "")
  )
  ; Check if parameters are valid.
  (if (not (pair? zones)) (error "Invalid parameter!" zones))
  (if (or (not (pair? direction)) (< (length direction) 3))
    (error "Invalid parameter, a vector (list) with 3 components is expected!" 
      direction)
  )

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; all-wall-zones parameter passed.
    (set! all-wall-zones (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (boolean? all-wall-zones))
      (error "Invalid parameter!" all-wall-zones)
    )
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; output-file parameter passed.
    (set! output-file (list-ref args 1))
    ; Check if parameter is valid.
    (if (and (not (string? output-file)) (not (symbol? output-file)))
      (error "Invalid parameter!" output-file)
    )
  ))

  ; Build the tui command string.
  ; The command parameters in the order the "wall-forces" command will ask 
  ; for them:
  ;   all wall zones? (yes|no)
  ;   zones list
  ;   x-coordinate of force vector
  ;   y-coordinate of force vector
  ;   z-coordinate of force vector
  ;   write to file? (yes|no)
  (set! tui-string (string-append
    tuicmd " "
    (if all-wall-zones
      "yes " ; use all wall zones for calculation
      (string-append "no " ; use only zones in list
        (string-list->tui-list (utils/object-list->string-list zones)) " ")
    )
    (utils/string-join (utils/object-list->string-list direction) " ") " "
    (if (> (string-length output-file) 0)
      (string-append ; output file passed as parameter
        "yes \"" output-file "\""
        ; If output file exists, overwrite it.
        (if (file-exists? output-file) " no ok" "")
      )
      "no" ; no output file
    )
  ))

  ; Execute the tui command.
  (ti-menu-load-string tui-string)
  (newline)
))

;*******************************************************************************
; Creates the mesh size report.  
; Tui command used: `/mesh/size-info`
(define (mesh-size-report)
  (ti-menu-load-string "/mesh/size-info")
  (newline)
)

;*******************************************************************************
; Calculates the mass flow report for the given zones.  
; Tui command used: `/report/fluxes/mass-flow`
;
; Parameters:
;   zones - The list of zones for which the mass flow should be calculated. This
;     must be a list of symbols and each symbol must be a valid zone name.
;   all-boundary-zones - [optional, default: #f] When #t is passed, then all 
;     boundary zones will included in the calculation.
;   output-file - [optional, default: ""] When the mass flow report should be 
;     written to a file, than a valid file path must be passed. Existing files
;     will be overwritten.
(define (mass-flow-report zones . args) (let (
    (tui-string "")
    (tuicmd "/report/fluxes/mass-flow")
    (all-boundary-zones #f)
    (output-file "")
  )
  ; Check if zones parameter is valid.
  (if (not (pair? zones)) (error "Invalid parameter!" zones))

  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; all-boundary-zones parameter passed.
    (set! all-boundary-zones (list-ref args 0))
    ; Check if parameter is valid.
    (if (not (boolean? all-boundary-zones))
      (error "Invalid parameter!" all-boundary-zones)
    )
  ))

  ; Parse optional arguments.
  (if (> (length args) 1) (begin
    ; output-file parameter passed.
    (set! output-file (list-ref args 1))
    ; Check if parameter is valid.
    (if (and (not (string? output-file)) (not (symbol? output-file)))
      (error "Invalid parameter!" output-file)
    )
  ))

  ; Build the tui command string.
  (set! tui-string (string-append
    tuicmd " "
    (if all-boundary-zones
      "yes " ; use all boundary zones for calculation
      ; use only zones in list
      (string-append "no " (string-list->tui-list zones) " ")
    )
    (if (> (string-length output-file) 0)
      (string-append ; output file passed as parameter
        "yes \"" output-file "\""
        ; If output file exists, overwrite it.
        (if (file-exists? output-file) " no ok" "")
      )
      "no" ; no output file
    )
  ))

  ; Execute the tui command.
  (ti-menu-load-string tui-string)
  (newline)
))

;*******************************************************************************
; Generates a summary report.  
; Tui command used: `/report/summary`
;
; Parameters:
;   output-file - [optional, default: ""] When the summary report should be 
;     written to a file, than a valid file path must be passed. Existing files
;     will be overwritten.
(define (settings-summary-report . args) (let (
    (tui-string "")
    (tuicmd "/report/summary")
    (output-file "")
  )
  ; Parse optional arguments.
  (if (> (length args) 0) (begin
    ; output-file parameter passed.
    (set! output-file (list-ref args 0))
    ; Check if parameter is valid.
    (if (and (not (string? output-file)) (not (symbol? output-file)))
      (error "Invalid parameter!" output-file)
    )
  ))

  ; Build the tui command string.
  (set! tui-string (string-append
    tuicmd " "
    (if (> (string-length output-file) 0)
      (string-append ; output file passed as parameter
        "yes \"" output-file "\""
        ; If output file exists, overwrite it.
        (if (file-exists? output-file) " ok" "")
      )
      "no" ; no output file
    )
  ))

  ; Execute the tui command.
  (ti-menu-load-string tui-string)
  (newline)
))

;*******************************************************************************
; Generates an area weighted average surface integral report for the given 
; property.  
; Tui command used: `/report/surface-integrals/area-weighted-avg`  
;
; Parameters:
;   surfaces - A list of surface names, for which the report should be created.
;   aavg-of - The property for which the report should be generated, for 
;     example `"pressure"` or `"velocity-magnitude"`. For a full list of 
;     possible properties, execute the tui command 
;     `/report/surface-integrals/area-weighted-avg`, then when asked 
;     `area-weighted-avg of>` press enter and you get a list of possible 
;     properties. 
(define (report-surf-aavg surfaces aavg-of) (let (
    (tui-cmd "/report/surface-integrals/area-weighted-avg")
  )
  ; Check if parameters are valid.
  (if (not (pair? surfaces)) (error "Invalid parameter!" surfaces))
  (if (symbol? aavg-of) (set! aavg-of (utils/to-string aavg-of)))
  (if (not (string? aavg-of)) (error "Invalid parameter!" aavg-of))

  ; Execute tui command.
  (ti-menu-load-string (string-append
    tui-cmd " "
    (string-list->tui-list (map (lambda (surface)
      (if (not (symbol? surface))
        (set! surface (string->symbol (utils/to-string surface)))
      )
      (utils/to-string (surface-name->id surface))
    ) surfaces)) " "
    aavg-of " "
    "no"
  ))
  (newline)
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'wall-moments-report wall-moments-report)
  (list 'wall-forces-report wall-forces-report)
  (list 'mesh-size-report mesh-size-report)
  (list 'mass-flow-report mass-flow-report)
  (list 'settings-summary-report settings-summary-report)
  (list 'report-surf-aavg report-surf-aavg)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
